import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * WEEK 09 - HIBERNATE
 * Submit the Topic Assignment for hibernate.
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-11-07
 *
 * <p>
 * REQUIREMENTS:
 * A model class (representing something like an employee or a book or something)
 * Database table (which database you use is not super important as long as it works with Hibernate -
 * you will probably find a lot of tutorials for MySQL though.
 * Configure hibernate to know how the model class maps to the database table.
 * This can be done with a mapping config xml file, or with annotations on the model class itself.
 * A hibernate config file, which will be an xml file that has all the setup to connect hibernate and your database.
 * You program will need to work with all these pieces, and I would like to see you retrieve data from your database
 * as well as adding data to your database.
 * Don't forget error handling/data validation.
 */
public class Main {

    // Set the default messages and values
    private static final String WHAT_PROGRAM_DOES_MSG = "\nThis program manages a list of prophets of the current dispensation.";
    private static final String WHAT_LIKE_TODO_MSG = "\nWhat would you like to do?";
    private static final String REGISTER_PROPHET_MSG = "\n   1. Register a Prophet in the database;";
    private static final String BY_ID_PROPHET_MSG = "\n   2. Find a prophet by its calling position;";
    private static final String LIST_PROPHET_MSG = "\n   3. See a list of prophets of this dispensation;";
    private static final String CLOSE_MSG = "\n   4. Close application.";
    private static final String INSERT_OPTION_MSG = "\nYour option: ";
    private static final String INVALID_INTEGER_MSG = "Please, only numbers are accepted!\n";
    private static final String INVALID_INPUT_MSG = "Invalid input!\n";
    private static final String INSERT_PROPHET_NAME_MSG = "Insert the Prophet's name: ";
    private static final String INSERT_PROPHET_AGE_MSG = "Insert the Prophet's age when he died (years): ";
    private static final String INSERT_YEAR_PRESIDED_MSG = "Insert the period of time (years) he presided the church: ";
    private static final String WHAT_POSITION_MSG = "\nWhat prophet position: ";
    private static final String CURRENT_POSITION_MSG = "\nOnly 17 prophets were called in this dispensation.";
    private static final String SOMETHING_WENT_WRONG_MSG = "\nSomething went wrong. Try again later.";
    private static final String NO_PROPHET_VALID_MSG = "\nNo prophet found in that position.";
    private static final String NO_DATA_MSG = "\nNo data in the database!";
    private static final String GOODBYE_MSG = "\nGoodbye ...";

    // Scanner input
    private static Scanner sc = new Scanner(System.in);

    /**
     * This function gets an option from the user to be executed in the program.
     *
     * @return the option chosen by the user.
     */
    private static int getOptionFromUser() {
    
        int optionInput = 0;

        do {
            // Output the options
            System.out.print(WHAT_LIKE_TODO_MSG);
            System.out.print(REGISTER_PROPHET_MSG);
            System.out.print(BY_ID_PROPHET_MSG);
            System.out.print(LIST_PROPHET_MSG);
            System.out.println(CLOSE_MSG);
            System.out.print(INSERT_OPTION_MSG);

            // Check if the input is valid
            try {
                optionInput = sc.nextInt();
            } catch (InputMismatchException ex) {
                sc.next();
                System.out.print(INVALID_INTEGER_MSG);
            } finally {
                sc.skip("(\r\n|[\n\r\u2028\u2029\u0085])?"); // This will skip spurious as well as carriage return.
            }
            // If the user does type a number out of option range, it will keep asking the user for an option.
        } while (optionInput < 1 || optionInput > 4);

        return optionInput;
        // Result: Option menu properly chosen by the user and validated.
    }

    /**
     * This function gets the position of the prophet.
     *
     * @return
     */
    private static int getProphetPosition() {
        int position = 0;

        do {
            // Output the options to retrieve data from the database
            System.out.print(WHAT_POSITION_MSG);

            // Check if the input is valid
            try {
                position = sc.nextInt();

                // Russel M. Nelson is the 17th prophet, so higher does not exist.
                if (position > 17) {
                    System.out.println(CURRENT_POSITION_MSG);
                }
            } catch (InputMismatchException ex) {
                sc.next(); // ignore any content in the Scanner object.
                System.out.print(INVALID_INTEGER_MSG);
            }
            // While the user has chosen a wrong prophet position
        } while (position < 1 || position > 17);

        return position;
        // Result: Position of the chosen prophet inserted by the user.
    }

    /**
     * This function gets a prophet from the user.
     *
     * @return a prophet object
     */
    private static Prophet getProphet() {

        Prophet prophet = new Prophet();
        String name = "";
        int deathAge = 0;
        int presidentLength = 0;
        boolean validInput = false;

        do {

            // Check if the input is valid
            try {
                // Output the options
                System.out.print(INSERT_PROPHET_NAME_MSG);
                name = sc.nextLine();

                System.out.print(INSERT_PROPHET_AGE_MSG);
                deathAge = sc.nextInt();

                System.out.print(INSERT_YEAR_PRESIDED_MSG);
                presidentLength = sc.nextInt();

                validInput = true;
            } catch (InputMismatchException ex) {
                sc.next(); // Ignore any content in the Scanner object
                System.out.println(INVALID_INPUT_MSG);
            }finally {
                sc.skip("(\r\n|[\n\r\u2028\u2029\u0085])?"); // This will skip spurious as well as carriage return.
            }
            // While the user has chosen a wrong position
        } while (!validInput);

        prophet.setName(name);
        prophet.setDeathAge(deathAge);
        prophet.setPresidentLength(presidentLength);

        return prophet;
        // Result: Prophet object properly validated!
    }

    /**
     * This function saves a prophet object to the database.
     *
     * @param prophet is the object to be saved.
     */
    public static void saveProphet(Prophet prophet) {
        // This interface is used to interact with the entity manager factory for the persistence unit.
        // the org.hibernate.tutorial.jpa was defined in the persistence.xml file.
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        // Entity Manager is used to create and remove persistent entity instances to find entities by their key or to query over entities.
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {
            entityManager.getTransaction().begin(); // It begins the transaction/connection with the database
            entityManager.persist(prophet); // It saves the data (prophet object) in the database
            entityManager.getTransaction().commit(); // It commits the transaction
        } catch (IllegalStateException ex) {
            entityManager.getTransaction().rollback(); // It rollbacks the transaction if an error occurs.
            ex.getStackTrace();
            System.out.println(SOMETHING_WENT_WRONG_MSG);
        } finally {
            entityManagerFactory.close(); // It closes the Manager Factory instance.
            entityManager.close(); // It closes the Manager instance.
        }

        System.out.println("\nProphet saved in the database.");
        // Result: Prophet data is saved in the database or an error message is displayed if something goes wrong.
    }

    /**
     * This function retrieves a prophet from the database by id.
     *
     * @param id of the prophet in the datatable.
     * @return a prophet object.
     */
    public static Prophet findProphetById(int id) {
        // This interface is used to interact with the entity manager factory for the persistence unit.
        // the org.hibernate.tutorial.jpa was defined in the persistence.xml file.
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        // Entity Manager is used to create and remove persistent entity instances to find entities by their key or to query over entities.
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Prophet prophet = new Prophet();

        try {
            entityManager.getTransaction().begin(); // It begins the transaction/connection with the database.
            prophet = entityManager.find(Prophet.class, id); // It deserialize the data from the database to a prophet class.
        } catch (IllegalStateException ex) {
            ex.getStackTrace();
            System.out.println(SOMETHING_WENT_WRONG_MSG);
        } finally {
            entityManagerFactory.close(); // It closes the Manager Factory instance.
            entityManager.close(); // It closes the Manager instance.
        }

        return prophet;
        // Result: A prophet is retrieved from the database according to its id (position calling in this dispensation)
    }

    /**
     * This function retrieves a list of prophets from the database.
     *
     * @return
     */
    public static List<Prophet> findAllProphets() {
        // This interface is used to interact with the entity manager factory for the persistence unit.
        // the org.hibernate.tutorial.jpa was defined in the persistence.xml file.
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        // Entity Manager is used to create and remove persistent entity instances to find entities by their key or to query over entities.
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Prophet> prophets = null;
        String query = "SELECT * FROM tb_prophet"; // Simple SQL query to retrieve all data form the table tb_prophet.

        try {
            entityManager.getTransaction().begin(); // It begins the transaction/connection with the database.
            // It retrieves all data from the table and saves it in the prophet object list.
            prophets = entityManager.createNativeQuery(query, Prophet.class).getResultList();
        } catch (IllegalStateException ex) {
            ex.getStackTrace();
            System.out.println(SOMETHING_WENT_WRONG_MSG);
        } finally {
            entityManagerFactory.close(); // It closes the Manager Factory instance.
            entityManager.close(); // It closes the Manager instance.
        }

        return prophets;
        // Result: A list of prophets is retrieved from the database.
    }

    /**
     * This function displays all the prophets on the screen.
     *
     * @param prophetList is a list with all prophets objects.
     */
    public static void displayProphetList(List<Prophet> prophetList) {
        System.out.println("\n\n\n\n");
        for (Prophet prophet : prophetList) {
            System.out.println(prophet.toString());
        }
        // Result: All prophets are displayed on the screen.
    }

    /**
     * This method defines the entry point for the program.
     *
     * @param args to be passed to the main() method
     */
    public static void main(String[] args) {

        int option = 4;

        // Display the header
        System.out.println(WHAT_PROGRAM_DOES_MSG);

        // Show Menu and Get Option From User
        do {
            option = getOptionFromUser();
            switch (option) {
                case 1:
                    Prophet prophet = getProphet();
                    saveProphet(prophet);
                    // Result: Prophet saved in the database
                    break;
                case 2:
                    int position = getProphetPosition();
                    Prophet foundProphet = findProphetById(position);
                    if(foundProphet != null)
                        System.out.println("\n\n\n\n" + foundProphet.toString());
                    else
                        System.out.println("\n\n\n\n" + NO_PROPHET_VALID_MSG);
                    // Result: Prophet retrieved from the database according to prophet's position and displayed on the screen.
                    break;
                case 3:
                    List<Prophet> prophetList = findAllProphets();
                    if(prophetList.size() != 0)
                        displayProphetList(prophetList);
                    else
                        System.out.println("\n\n\n\n" + NO_DATA_MSG);
                    // Result: All prophets registered in the database is retrieved and displayed on the screen.
                    break;
            }
        } while (option != 4); // Only four options are available in this program.

        // Display Goodbye message
        System.out.println(GOODBYE_MSG);
    }
}
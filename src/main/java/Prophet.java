import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * PROPHET CLASS
 * This class defines the model class maps to the database table.
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-11-07
 *
 * <p>
 * It uses annotations on the model class to map the database and the fields of the class.
 */
@Entity // This class is being used as bean to hold prophet information data.
@Table(name = "tb_prophet") // It defines the name of the table this class refers to
public class Prophet implements Serializable {

    /**
     * Default constructor
     */
    public Prophet() {
        this.id = 0;
        this.name = "";
        this.deathAge = 0;
        this.presidentLength = 0;
    }

    /**
     * Non-default constructor
     *
     * @param name            of the prophet
     * @param deathAge        age when the prophet died
     * @param presidentLength time the prophet stayed as president of the church
     */
    public Prophet(String name, int deathAge, int presidentLength) {
        this.name = name;
        this.deathAge = deathAge;
        this.presidentLength = presidentLength;
    }

    @Id // it specifies the column and field as primary key.
    @GeneratedValue(generator = "incrementor") // it selects max (id) from the table.
    @GenericGenerator(name = "incrementor", strategy = "increment") // It defines that the ID will be automatically generated by the class.
    @Column(name = "id") // it defines the name of the column in the table inside the database.
    private int id;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name") private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "deathAge") private int deathAge;
    public int getDeathAge() {
        return deathAge;
    }
    public void setDeathAge(int deathAge) {
        this.deathAge = deathAge;
    }

    @Column(name = "presidentLength") private int presidentLength;
    public int getPresidentLength() {
        return presidentLength;
    }
    public void setPresidentLength(int presidentLength) {
        this.presidentLength = presidentLength;
    }

    /**
     * This function returns the prophet string format.
     *
     * @return prophet string format.
     */
    public String toString(){
        return "Position: " + this.id + " - " + this.name +
                " - Death Age: " + this.deathAge +
                " - Presided the church for over " + this.presidentLength + " years.";
    }
}
